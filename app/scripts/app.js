$(() => {

    let intervalId;

    $('.gameOfLifeGridContainer').on('click', '.golCell', LifeUi.toggleTableCellStatus);

    $('#golStart').on('click', (event) => {
        let golarray = LifeUi.createArrayFromUI();
        let game = new Life(golarray);
        if ($("#chkAutoPlay").prop("checked")) {
            intervalId = setInterval(() => {
                let gameBoard = game.board;
                game.next();
                //compareTwoArrays(gameBoard, game.board);
                LifeUi.renderUi(game.board);
            }, 1000);
        } else {
            game.next();
            LifeUi.renderUi(game.board);
        }
    });

    $('#chkAutoPlay').on('click', (event) => {
        if(!$(event.target).prop('checked')) {
            clearInterval(intervalId);
        }
    });

    $('button.golCreateGrid').on('click', (event) => {
         LifeUi.createGrid($('.golGridHeight').val(), $('.golGridWidth').val());
    }); 
});