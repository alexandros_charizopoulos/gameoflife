class Life {
    constructor(seed) {
        this.seed = seed;
        this.height = seed.length;
        this.width = seed[0].length;
        
        this.prevBoard = [];
        this.board = this.cloneArray(seed);
    }
    
    next() {
       this.prevBoard = this.cloneArray(this.board);
       for(let y = 0; y < this.height; y++) {
           for(let x = 0; x < this.width; x++) {
               let neighbors = this.aliveNeighbors(this.prevBoard, x, y);
               let alive = !!this.board[y][x];
               if (alive) {
                   if (neighbors < 2 || neighbors > 3) {
                       this.board[y][x] = 0;
                   }
               } else {
                   if (neighbors === 3) {                       
                       this.board[y][x] = 1;
                   }
               }
           }
       }
    }
    
    aliveNeighbors(array, x, y) {
        let sum = 0, 
        prevRow = array[y-1] || [],
        nextRow = array[y+1] || [];
        return [
          prevRow[x-1], prevRow[x], prevRow[x+1],
          array[y][x-1], array[y][x+1],
          nextRow[x-1], nextRow[x], nextRow[x+1]
        ].reduce((prev, cur) => {
            return prev + +!!cur;
        });
    }
    
    cloneArray(array) {
        return array.slice().map(row => { return row.slice(); });
    }
    
    toString(array) {
        return array ? array.map( row => { return row.join('') }).join('\n') : this.board.map( row => { return row.join('') }).join('\n');
    }
}