class LifeUi {

    static toggleTableCellStatus(event) {
        $(event.currentTarget).toggleClass('alive');
    };

    static createArrayFromUI() {
        
        let arr = [], k = -1, tableHeight = $('.golRow').length;
        
        $('.golCell').each((i, e) => {
            if (i === 0 || i % tableHeight === 0) {
                arr.push([]);
                k++;
            }
            if ($(e).hasClass('alive')) {
                arr[k].push(1);
            } else {
                arr[k].push(0);
            }
        });
        return arr;
    };

    static renderUi(arr) {

        let k = 0, l = 0, tableHeight = $('.golRow').length;
        
        $('.golCell').each((i, e) => {
            if (i % tableHeight === 0 && i !== 0) {
                k++;
                l = 0;
            }
            if (arr[k][l] === 1) {
                $(e).addClass('alive');
            } else {
                $(e).removeClass('alive');
            }
            l++;
        });
    };

    static createGrid(height, width) {

        let gridTemplate = _.template($('.golGridTemplate').html());

        $('div.gameOfLifeGridContainer').html(gridTemplate({
            width: parseInt(width, 10),
            height: parseInt(height, 10)
        }));

    }

}