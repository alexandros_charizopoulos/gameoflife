(function () {
  'use strict';
  describe('Game of life tests', function () {
    describe('Oscillators patterns', function () {
      it('Blinker (period 2)', () => {
        let game = new Life([
          [0, 0, 0, 0, 0],
          [0, 0, 1, 0, 0],
          [0, 0, 1, 0, 0],
          [0, 0, 1, 0, 0],
          [0, 0, 0, 0, 0]
        ]);
        game.next();
        assert.equal(game.board[2][1], 1);
        assert.equal(game.board[2][2], 1);
        assert.equal(game.board[2][3], 1);
        game.next();
        assert.equal(game.board[1][2], 1);
        assert.equal(game.board[2][2], 1);
        assert.equal(game.board[3][2], 1);
      });
      it('Toad (period 2)', () => {
        let game = new Life([
          [0, 0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0, 0],
          [0, 0, 1, 1, 1, 0],
          [0, 1, 1, 1, 0, 0],
          [0, 0, 0, 0, 0, 0],
          [0, 0, 0, 0, 0, 0]
        ]);
        game.next();
        assert.equal(game.board[1][3], 1);
        assert.equal(game.board[2][1], 1);
        assert.equal(game.board[2][4], 1);
        assert.equal(game.board[3][1], 1);
        assert.equal(game.board[3][4], 1);
        assert.equal(game.board[4][2], 1);
        game.next();
        assert.equal(game.board[2][2], 1);
        assert.equal(game.board[2][3], 1);
        assert.equal(game.board[2][4], 1);
        assert.equal(game.board[3][1], 1);
        assert.equal(game.board[3][2], 1);
        assert.equal(game.board[3][3], 1);
      })
    });
  });
})();
